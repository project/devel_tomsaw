<?php

namespace Drupal\devel_tomsaw\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Musubi Service event subscriber.
 */
class DevelTomSawCacheObserver implements EventSubscriberInterface {
  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      // KernelEvents::RESPONSE => ['onKernelResponse'],
    ];
  }

  public function onKernelResponse(ResponseEvent $event) {
    $response = $event->getResponse();
    $headers = $response->headers;

    // #####################
    // # Control header generation in services.yml: http.response.debug_cacheability_headers = false/true
    // # Alternatively: Check Chrome -> Devtools -> Network -> File-Type: Document -> Sidepane: Headers
    // #####################
    $headers_observ = [];
    foreach (['x-drupal-cache-tags', 'x-drupal-cache-contexts'] as $header) {
      if ($headers->has($header)) {
        $headers_observ[$header] = explode(' ', $headers->get($header));
      }
    }
    vardumper($headers_observ, 'Response with x-drupal- headers');
  }
}
