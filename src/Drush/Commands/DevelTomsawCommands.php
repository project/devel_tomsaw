<?php

namespace Drupal\devel_tomsaw\Drush\Commands;

use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Drupal\memcache\Driver\MemcacheDriverFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
final class DevelTomsawCommands extends DrushCommands {

  /**
   * Constructs a DevelTomsawCommands object.
   */
  public function __construct(
    private readonly MemcacheDriverFactory $memcacheFactory
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('memcache.factory')
    );
  }

  #[CLI\Command(name: 'cache:memcache-flush', aliases: ['cmf'])]
  public function flushMemcache() {
    // @todo check if memcache exists!
    $this->memcacheFactory->get('default')->flush();
    $this->logger()->success("Memcache flushed");
  }
}
